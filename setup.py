'''
                                  ESP Health
                          Diabetes Disease Definition
                             Packaging Information
                                  
@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics http://www.commoninf.com
@contact: http://esphealth.org
@copyright: (c) 2017 Harvard Department of Population Medicine
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-diabetes',
    version = '1.8',
    author = 'Bob Zambarano',
    author_email = 'bzambarano@commoninf.com',
    description = 'Diabetes disease definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'diabetes algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = diabetes:disease_definitions
        event_heuristics = diabetes:event_heuristics
        timespan_heuristics = diabetes:timespan_heuristics
        reports = diabetes:reports
    '''
    )
