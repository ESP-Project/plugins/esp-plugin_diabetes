'''
                                  ESP Health
                         Notifiable Diseases Framework
                            Diabetes Case Generator


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org 
@copyright: (c) 2010-2011 Channing Laboratory
@license: LGPL
'''

from ESP.emr.models import Encounter, LabOrder, LabResult, Patient, Prescription, Pregnancy
from ESP.conf.models import LabTestMap
from ESP.hef.base import AbstractLabTest, BaseHeuristic, DiagnosisHeuristic, \
    Dose, Dx_CodeQuery, LabOrderHeuristic, LabResultAnyHeuristic, \
    LabResultFixedThresholdHeuristic, LabResultPositiveHeuristic, \
    LabResultRangeHeuristic, LabResultRatioHeuristic, LabResultWesternBlotHeuristic, \
    PrescriptionHeuristic, LabResultNoEventHeuristic
from ESP.hef.models import Event, Timespan

from ESP.static.models import DrugSynonym
from ESP.nodis.base import DiseaseDefinition, Report
from ESP.nodis.models import Case, CaseActiveHistory
from ESP.utils.utils import log
from ESP.utils.utils import log_query, binary
from ESP.utils.utils import TODAY
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.db.models import Avg, Count, F, Max, Min, Q, Sum
from django.utils.encoding import smart_str
from django.contrib.contenttypes.models import ContentType
import csv
import datetime
import re
import sys
from functools import partial
from multiprocessing import Queue
from ESP.utils.utils import wait_for_threads


class Diabetes(DiseaseDefinition):
    '''
    Diabetes Mellitus:
    - Pre-diabetes
    - Frank Diabetes
    - Gestational Diabetes
    '''
    
    conditions = [
        'diabetes:prediabetes',
        'diabetes:type-1',
        'diabetes:type-2',
        'diabetes:gestational'
        ]
    
    uri = 'urn:x-esphealth:disease:channing:diabetes:v1'
    
    short_name = 'diabetes'

        def update_drugsyn(self):
        druglist=(
                  ('acarbose','acorbose','Self'),
                  ('acarbose','precose',None),
                  ('miglitol','miglitol','Self'),
                  ('miglitol','glyset',None),
                  ('Pramlintide','Pramlintide','Self'), 
                  ('Pramlintide','Symlin',None),
                  ('Alogliptin','Alogliptin','Self'),
                  ('Alogliptin','Nesina',None),
                  ('Alogliptin','Kazano',None),
                  ('Alogliptin','Oseni',None), 
                  ('Linagliptin','Linagliptin','Self'),
                  ('Linagliptin','Tradjenta',None),
                  ('Linagliptin','trajenta',None),
                  ('Linagliptin','Glyxambi',None),
                  ('Linagliptin','Trijardy',None), 
                  ('Linagliptin','Jentadueto',None),
                  ('Saxagliptin','Saxagliptin','Self'),
                  ('Saxagliptin','Onglyza',None),
                  ('Saxagliptin','Kombiglyze',None),
                  ('Saxagliptin','Qternmet',None),
                  ('Saxagliptin','Qtern',None), 
                  ('Sitagliptin','Sitagliptin','Self'),
                  ('Sitagliptin','Januvia',None),
                  ('Sitagliptin','Janumet',None),
                  ('Sitagliptin','Steglujan',None), 
                  ('Tirzepatide','Tirzepatide','Self'),
                  ('Tirzepatide','Mounjaro',None),
                  ('Albiglutide','Albiglutide','Self'),
                  ('Albiglutide','Tanzeum',None),
                  ('Dulaglutide','Dulaglutide','Self'),
                  ('Dulaglutide','Trulicity',None),
                  ('Exenatide','Exenatide','Self'),
                  ('Exenatide','Byetta',None),
                  ('Exenatide','Bydureon',None),
                  ('Liraglutide','Liraglutide','Self'),
                  ('Liraglutide','Victoza',None),
                  ('Liraglutide','Saxenda',None),
                  ('Liraglutide','Xultophy',None),
                  ('Lixisenatide','Lixisenatide','Self'), 
                  ('Lixisenatide','Adlyxin',None),
                  ('Lixisenatide','Soliqua',None),
                  ('Semaglutide','Semaglutide','Self'), 
                  ('Semaglutide','Ozempic',None),
                  ('Semaglutide','Rybelsus',None),
                  ('Insulin','Insulin','Self'), 
                  ('Insulin','Afrezza',None),
                  ('Insulin','Myxredlin',None),
                  ('Insulin','Fiasp',None), 
                  ('Insulin','Novolog',None), 
                  ('Insulin','Tresiba',None), 
                  ('Insulin','Levemir',None), 
                  ('Insulin','Basaglar',None),
                  ('Insulin','Lantus',None), 
                  ('Insulin','Rezvoglar',None),
                  ('Insulin','Semglee',None), 
                  ('Insulin','Toujeo',None), 
                  ('Insulin','Apidra',None),
                  ('Insulin','Humulin',None), 
                  ('Insulin','Novolin',None), 
                  ('Insulin','Admelog',None), 
                  ('Insulin','Humalog',None),
                  ('Insulin','Lyumjev',None), 
                  ('Insulin','Soliqua',None),
                  ('Insulin','Xultophy',None), 
                  ('Nateglinide','Nateglinide','Self'),
                  ('Nateglinide','Starlix',None),
                  ('Repaglinide','Repaglinide','Self'), 
                  ('Repaglinide','Prandin',None), 
                  ('Repaglinide','Novonorm',None), 
                  ('Repaglinide','Prandimet',None),
                  ('Canagliflozin','Canagliflozin','Self'), 
                  ('Canagliflozin','Invokana',None), 
                  ('Canagliflozin','Invokamet',None),
                  ('Dapagliflozin','Dapagliflozin','Self'), 
                  ('Dapagliflozin','Farxiga',None),
                  ('Dapagliflozin','Xigduo',None),
                  ('Dapagliflozin','Qternmet',None), 
                  ('Dapagliflozin','Qtern',None), 
                  ('Empagliflozin','Empagliflozin','Self'), 
                  ('Empagliflozin','Jardiance',None), 
                  ('Empagliflozin','Glyxambi',None),
                  ('Empagliflozin','Trijardy',None), 
                  ('Empagliflozin','Synjardy',None),
                  ('Ertugliflozin','Ertugliflozin','Self'), 
                  ('Ertugliflozin','Steglatro',None),
                  ('Ertugliflozin','Segluromet',None),
                  ('Ertugliflozin','Steglujan',None), 
                  ('Glimepiride','Glimepiride','Self'), 
                  ('Glimepiride','Amaryl',None), 
                  ('Glimepiride','Duetact',None),
                  ('Glipizide','Glipizide','Self'),
                  ('Glipizide','Glucotrol',None),
                  ('Glipizide','Metaglip',None),
                  ('Glyburide','Glyburide','Self'),
                  ('Glyburide','Calabren',None), 
                  ('Glyburide','Daonil',None), 
                  ('Glyburide','Diabeta',None), 
                  ('Glyburide','Diabetamide',None), 
                  ('Glyburide','Euglucon',None), 
                  ('Glyburide','Glyburase',None), 
                  ('Glyburide','Glynase',None), 
                  ('Glyburide','Libanil',None), 
                  ('Glyburide','Malix',None), 
                  ('Glyburide','Micronase',None), 
                  ('Glyburide','Glucovance',None),
                  ('Pioglitazone','Pioglitazone','Self'),  
                  ('Pioglitazone','Actos',None), 
                  ('Pioglitazone','Actoplus',None),
                  ('Pioglitazone','Duetact',None),
                  ('Pioglitazone','Oseni',None), 
                  ('Rosiglitazone','Rosiglitazone','Self'),
                  ('Rosiglitazone','Avandia',None),
                  ('Rosiglitazone','Avandamet',None))
        for syntup in druglist:
               drugrow, created = DrugSynonym.objects.get_or_create(generic_name__iexact=syntup[0],other_name__iexact=syntup[1],comment=syntup[2])
               if created:
                   drugrow.save()
    
    def generate(self):
        counter = 0
        counter += self.generate_prediabetes()
        counter += self.generate_frank_diabetes()
        counter += self.update_case_active_status()
        counter += self.generate_gestational_diabetes()
        return counter
    
    @property
    def event_heuristics(self):

        self.update_drugsyn()

        heuristics = []
        
        ''' sample of other kind of abstract lab creation 
        heuristics.append(AbstractLabTest.objects.get_or_create(
            name = 'cholesterol-hdl',
            defaults = {
                    'verbose_name': 'cholesterol-hdl',
                        }
            )[0])
        '''
        #
        # Threshold Tests
        #
        for test_name, match_type, threshold in [
                       
            # Fasting OGTT
            ('ogtt50-fasting', 'gte', 126), 
            ('ogtt100-fasting', 'gte', 126),
            ('glucose-fasting', 'gte', 126),
            # OGTT50
            ('ogtt50-1hr', 'gte', 190),
            ('ogtt50-random', 'gte', 190),
            ('ogtt50-random', 'gte', 200),
            ('glucose-random','gte', 200),
            # OGTT75
            ('ogtt75-fasting', 'gte', 92),
            ('ogtt75-fasting', 'gte', 126),
            ('ogtt75-30min', 'gte', 200),
            ('ogtt75-1hr', 'gte', 180),
            ('ogtt75-1hr', 'gte', 200),
            ('ogtt75-90min', 'gte', 180),
            ('ogtt75-90min', 'gte', 200),
            ('ogtt75-2hr', 'gte', 153),
            ('ogtt75-2hr', 'gte', 200),
            # OGTT100
            ('ogtt100-fasting', 'gte', 95),
            ('ogtt100-30min', 'gte', 200),
            ('ogtt100-1hr', 'gte', 180),
            ('ogtt100-90min', 'gte', 180),
            ('ogtt100-2hr', 'gte', 155),
            ('ogtt100-3hr', 'gte', 140),
            ('ogtt100-4hr', 'gte', 140),
            ('ogtt100-5hr', 'gte', 140),
             # Auto-antibodies
            ('gad65', 'gt', 1),
            ('ica512', 'gt', 0.8),
            ('islet-cell-antibody','gte', 1.25),
            # there is another auto-antibody with threshold of 0.4 see redmine
            ('insulin-antibody', 'gt', 0.8),
             # A1C
            ('a1c', 'gte', 6.5),
            # C-Peptide
            ('c-peptide', 'lt', 0.8),
            ]:
            h = LabResultFixedThresholdHeuristic(
                test_name = test_name,
                match_type = match_type,
                threshold = Decimal(str(threshold)),
                )
            heuristics.append(h)
        
        #
        # Range Tests
        #
        heuristics.append(LabResultRangeHeuristic(
                test_name = 'glucose-random',
                min = Decimal(str(140)),
                max = Decimal(str(200)),
                max_match = 'lt', ))
        
        for test_name, low, high in [
            ('a1c', 5.7, 6.4),
            ('glucose-fasting', 100, 125),
            ('ogtt50-fasting', 100, 125), 
            ('ogtt75-fasting', 100, 125),
            ('ogtt100-fasting', 100, 125),
            ('ogtt75-30min', 140, 199),
            ('ogtt75-1hr', 140, 199),
            ('ogtt75-90min', 140, 199),
            ('ogtt75-2hr', 140, 199),
            ]:
            h = LabResultRangeHeuristic(
                test_name = test_name,
                min = Decimal(str(low)),
                max = Decimal(str(high)),
                )
            heuristics.append(h)
        #
        # Encounters
        #
        heuristics.append (DiagnosisHeuristic(
            name = 'gestational-diabetes',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = '648.8', type='icd9'),
                Dx_CodeQuery(starts_with = 'O24.4', type='icd10'),
                
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:all-types',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = '250.', type='icd9'),
                Dx_CodeQuery(starts_with = 'E10', type='icd10'),
                Dx_CodeQuery(starts_with = 'E11', type='icd10'),
                Dx_CodeQuery(starts_with = 'E12', type='icd10'),
                Dx_CodeQuery(starts_with = 'E13', type='icd10'),
                Dx_CodeQuery(starts_with = 'E14', type='icd10'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:unspecified',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = 'E14', type='icd10'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:malnutrition-related',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = 'E12', type='icd10'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:other',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = 'E13', type='icd10'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:type-1-not-stated-icd9',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = '250.', ends_with='1', type='icd9'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:type-1-not-stated-icd10',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = 'E10', type='icd10'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:type-1-uncontrolled',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = '250.', ends_with='3', type='icd9'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:type-2-not-stated-icd9',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = '250.', ends_with='0', type='icd9'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:type-2-not-stated-icd10',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = 'E11', type='icd10'),
                ]
            ) )
        heuristics.append (DiagnosisHeuristic(
            name = 'diabetes:type-2-uncontrolled',
            dx_code_queries = [
                Dx_CodeQuery(starts_with = '250.', ends_with='2', type='icd9'),
                ]
            ) )
        
        #
        # Prescriptions
        #
        for drug in [
            'metformin', 
            'glyburide', 
            'test strips', 
            'lancets',
            'pramlintide',
            'exenatide',
            'sitagliptin',
            'meglitinide',
            'nateglinide',
            'repaglinide',
            'glimepiride',
            'glipizide',
            'gliclazide',
            'rosiglitizone',
            'pioglitazone',
            'acetone',
            'glucagon',
            'miglitol',
            'acarbose',
            'alogliptin',
            'linagliptin',
            'saxagliptin',
            'tirzepatide',
            'albiglutide',
            'dulaglutide',
            'liraglutide',
            'lixisenatide',
            'semaglutide',
            'canagliflozin',
            'dapagliflozin',
            'empagliflozin',
            'ertugliflozin',
            ]:
            h = PrescriptionHeuristic(
                name = drug.replace(' ', '-'),
                drugs = DrugSynonym.generics_plus_synonyms([drug]),
                exclude = ['lactos'],
                )
            heuristics.append(h)
            
        h = PrescriptionHeuristic(
            name = 'insulin',
            drugs = DrugSynonym.generics_plus_synonyms(['insulin']),
            exclude = ['syringe', 'mexiletine'],
            )
        heuristics.append(h)
        
        #
        # Misc
        #
        heuristics.append( LabResultPositiveHeuristic(
            test_name = 'islet-cell-antibody',
            titer_dilution = 4, # 1:4 titer
            ) )
        return heuristics
    
    @property
    def timespan_heuristics(self):
        return []
    
    test_name_search_strings = [
            'chol',
            'trig',
            'peptide',
            'ogtt',
            'gluc',
            'gad65',
            'ica512',
            'a1c',
            'ldl',
            'hdl',
            'lipoprotein',
            'islet',
            'insulin',
            'diab',
            'glyc',
        ]
    
    #-------------------------------------------------------------------------------
    #
    # Frank Diabetes
    #
    #-------------------------------------------------------------------------------
    __FIRST_YEAR = 2006
    __FRANK_DM_CONDITIONS = ['diabetes:type-1', 'diabetes:type-2']
    __ORAL_HYPOGLYCAEMICS = [
        'rx:metformin',
        'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide',
        'rx:miglitol'
        ]
    # If a patient has one of these events, he has frank diabetes
    __FRANK_DM_ONCE = [
        'lx:a1c:threshold:gte:6.5',
        # these below are all the fasting glucose
        'lx:glucose-fasting:threshold:gte:126',
        'lx:ogtt50-fasting:threshold:gte:126',
        'lx:ogtt75-fasting:threshold:gte:126',
        'lx:ogtt100-fasting:threshold:gte:126',
        
        'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide',
        ]
    # If a patient has one of these events twice, he has frank diabetes
    __FRANK_DM_TWICE = [
        'lx:glucose-random:threshold:gte:200',
        'lx:ogtt50-random:threshold:gte:200',
        'dx:diabetes:all-types',
        ]
    __FRANK_DM_ALL = __FRANK_DM_ONCE + __FRANK_DM_TWICE
    __AUTO_ANTIBODIES_LABS = [
        'lx:ica512:threshold:gt:0.8', #threshold
        'lx:gad65:threshold:gt:1', #threshold
        'lx:islet-cell-antibody:positive', # is a positive and threshold
        'lx:islet-cell-antibody:threshold:gte:1.25',
        'lx:insulin-antibody:threshold:gt:0.8' 
        ]
    
    def process_existing_cases(self, relevant_patients,patient_events,conditions):
        
        existing_cases = Case.objects.filter(
                patient__in = relevant_patients ,
                condition__in = conditions,
                ).order_by('date')

        #get distinct patients 
        patients = set()
        for case in existing_cases:
            events = []
            patients.add(case.patient_id)
            try:
                for event in patient_events[case.patient_id]:
                    events.append(Event.objects.get(pk=event['pk']))
                self._update_case_from_event_list(case,
                            relevant_events = events )
                if events and case.condition=='diabetes:type-2':
                    self.type_checker(case)
            except KeyError:
                pass
        return patients

    def type_checker(self,case):
        events=case.all_events
        condition = None
        case_event = None
        criteria = ''
         
        if any(event for event in events if event.name == 'lx:c-peptide:threshold:lt:0.8'):
            c_peptide_lx_thresh = sorted([event for event in events if event.name == 'lx:c-peptide:threshold:lt:0.8'], 
                                         key=lambda event: event.date)
            case_event = c_peptide_lx_thresh[0]
            criteria += ' (C-Peptide result below threshold)'
            condition = 'diabetes:type-1'
        
        if any(event for event in events if event.name in self.__AUTO_ANTIBODIES_LABS):
            aa_pos = sorted([event for event in events if event.name in self.__AUTO_ANTIBODIES_LABS], key=lambda event: event.date)
            case_event = aa_pos[0]
            criteria += ' (Diabetes auto-antibodies positive)'
            condition = 'diabetes:type-1'

        if any(event for event in events if event.name == 'rx:acetone'):
            acetone_rx = sorted([event for event in events if event.name == 'rx:acetone'], key=lambda event: event.date)
            case_event = acetone_rx[0]
            criteria += '(Acetone Rx)'
            condition = 'diabetes:type-1'
        
        oral_hypoglycaemic_rx = sorted([event for event in events if event.name in self.__ORAL_HYPOGLYCAEMICS], key=lambda event: event.date)
        glucagon_rx = [event for event in events if event.name =='rx:glucagon'] 
        if glucagon_rx or (not oral_hypoglycaemic_rx):
            type_1_dx = [event for event in events if event.name.startswith('dx:diabetes:type-1')]
            type_2_dx = [event for event in events if event.name.startswith('dx:diabetes:type-2')]  
            count_1 = len(type_1_dx)
            count_2 = len(type_2_dx)
            # Is there a less convoluted way to express this and still avoid divide-by-zero errors?
            if (count_1 and not count_2) or ( count_2 and ( ( count_1 / count_2 ) > 0.5 ) ):
                case_event = sorted(glucagon_rx + type_1_dx + type_2_dx, key=lambda event: event.date)[0]
                if glucagon_rx:
                    criteria += ' (More than 50% of dx_codes and glucagon rx)'
                else:
                    criteria += ' (More than 50% of dx_codes, and never prescribed oral hypoglycaemics)'
                condition = 'diabetes:type-1'
                
        if condition and case_event:
            case.notes = (case.notes or '') + " Initially identified as type-2 case" 
            case.condition = condition
            case.date = case_event.date
            case.isactive = True
            case.criteria = criteria
            case.provider = case_event.provider
            case.updated_timestamp = datetime.datetime.now()
            case.save()
            CaseActiveHistory.objects.filter(case=case).delete()
        
        return
    
    def update_case_active_status(self):
        pre_updatable_qs = Case.objects.filter(condition='diabetes:type-2').annotate(max_date=Max('caseactivehistory__latest_event_date')).annotate(max_init=Max('caseactivehistory__date'))
        q1 = Q(events__date__gt=F('max_date')) # events after current case history max event date
        q2 = Q(max_date__lt=datetime.datetime.now() - relativedelta(years=2)) # or max event date is older than 2 year
        updatable_qs = pre_updatable_qs.filter(q1 | q2 ).distinct()
        counter = 0 # track number of cases updated. Not currently bothering with case history last event updates, only active status changes.
        for case in updatable_qs:
            last_date = case.max_date
            events = case.events.filter(date__gt=last_date).order_by('date')
            disact_encs = Encounter.objects.none() #we check if this exists in the case.isactive=False condition.
            if case.isactive:
                disact_end = None
                updtcurhist = False
                updtevnt = None
                for event in events:  #might be no events see q2 above
                    if event.date < last_date + relativedelta(years=2): # assume most new events will just support current case
                        updtcurhist = True
                        updtevnt = event
                        last_date = event.date
                    else: # got here because we're on an event that isn't within 2 years of last qualifying event
                        disact_end = event.date + relativedelta(days=-1)
                        updtevnt = event
                        break  
                if updtcurhist: #we just saw one or more events that did qualify as last event, so update current history with that data
                    histupdt=CaseActiveHistory.objects.get(case=case,latest_event_date=case.max_date)
                    histupdt.latest_event_date=last_date
                    histupdt.content_type = ContentType.objects.get_for_model(updtevnt)
                    histupdt.object_id = updtevnt.pk
                    histupdt.save()
                if disact_end or last_date + relativedelta(years=2) < datetime.datetime.now().date(): #have evidence to disactivate case
                    disact_strt = last_date + relativedelta(years=2)
                    disact_end = (datetime.datetime.now() if not disact_end else disact_end)
                    disact_encs = Encounter.objects.filter(patient__exact=case.patient, date__range=(disact_strt,disact_end)).order_by('date')
                    if disact_encs.exists(): #found one or more disactivating encounters
                        disact_metform = Prescription.objects.filter(patient__exact=case.patient, 
                                                                     date__range=(disact_encs[0].date-relativedelta(years=2),disact_encs[0].date),
                                                                     name__in=DrugSynonym.generics_plus_synonyms(['metformin'])).order_by('date')
                        disact_a1c = AbstractLabTest('a1c').lab_results.filter(patient__exact=case.patient,
                                                                               date__lt=disact_encs[0].date,
                                                                               date__gt=case.max_init).order_by('-date')
                        if (not disact_metform.exists()) and disact_a1c.exists() and disact_a1c[0].result_float is not None and disact_a1c[0].result_float<6.5:
                            case.isactive = False
                            case.inactive_date = disact_encs[0].date
                            case.save()
                            newhist = CaseActiveHistory.create(case,
                                                               'D',
                                                               disact_encs[0].date,
                                                               'D',
                                                               disact_encs[0])
                            counter += 1
                    elif updtevnt and not updtcurhist:
                        histupdt = CaseActiveHistory.objects.get(case=case,latest_event_date=case.max_date)
                        histupdt.latest_event_date = updtevnt.date
                        histupdt.content_type = ContentType.objects.get_for_model(updtevnt)
                        histupdt.object_id = updtevnt.pk
                        histupdt.save()                       
            if not case.isactive:
                if disact_encs.exists():
                    events = events.filter(date__gt=disact_encs[0].date).order_by('date')
                else:
                    events = events.filter(date__gt=case.max_date).order_by('date')
                fdm_once = any(event for event in events if event.name in self.__FRANK_DM_ONCE)
                fdm_twice = False
                for item in self.__FRANK_DM_TWICE:
                    this_item_twice = (True if sum(1 for event in events if event.name==item)>=2 else False)
                    fdm_twice = (True if this_item_twice else fdm_twice)
                fdm_insulin_unpreg = any(event for event in events if (event.name=='rx:insulin' and not 
                                      Timespan.objects.filter(name = 'pregnancy', patient = event.patient, 
                                                              start_date__lte = event.date, end_date__gte = event.date).exists()))
                if fdm_once or fdm_twice or fdm_insulin_unpreg:
                    case.isactive=True
                    case.save()
                    CaseActiveHistory.create(case,
                                     'R',
                                     events[0].date,
                                     'Q',
                                     events[0])
                    log.debug('Reactivated case: %s' % (case))                        
                    counter += 1
        return counter


    def generate_frank_diabetes(self):
        log.info('Generating cases of frank diabetes type 1 and 2.')
        pat_date_events = {}  # {patient: {date: set([event, event, ...]), ...}
        #
        # Frank DM critiera
        #
        criteria = ''
        # Start with a query of event types which need only a single event to indicate a case of DM
        once_and_insulin = self.__FRANK_DM_ONCE + ['rx:insulin']
        once_qs = Event.objects.filter(name__in=once_and_insulin)
        once_qs = once_qs.exclude(case__condition__in=self.__FRANK_DM_CONDITIONS).values('pk','patient','name','date')
        if once_qs:
            criteria = 'Criteria #1: Hemoglobin A1C >= 6.5 or Fasting glucose (FG) >=126 or prescription for oral hypoglycemics or (not pregnant and prescription for insulin)'
        dm_criteria_list = [once_qs]
        # Add event types which must occur >=2 times to indicate DM
        for event_name in self.__FRANK_DM_TWICE:
            twice_qs = Event.objects.filter(name=event_name).values('patient')
            twice_qs = twice_qs.exclude(case__condition__in=self.__FRANK_DM_CONDITIONS)
            twice_vqs = twice_qs.annotate(count=Count('pk'))
            twice_vqs = twice_vqs.filter(count__gte=2)
            twice_patients = twice_vqs.values_list('patient')
            twice_criteria_qs = Event.objects.filter(name=event_name, patient__in=twice_patients).exclude(case__condition__in=self.__FRANK_DM_CONDITIONS).values('pk','patient','name','date')
            dm_criteria_list.append(twice_criteria_qs)
        if twice_criteria_qs:
            criteria += 'Criteria #2: Random glucoses (RG) >=200 on two or more occasions, dx code for diabetes on two or more occasions'
        patients=set()
        patient_events={}
        for criteria_qs in dm_criteria_list:
            for this_event in criteria_qs:
                if this_event['name'] == 'rx:insulin' and Timespan.objects.filter(
                    name = 'pregnancy',
                    patient = this_event['patient'],
                    start_date__lte = this_event['date'],
                    end_date__gte = this_event['date'],
                    ):
                    continue # Exclude insulin during pregnancy
                try:
                    patient_events[this_event['patient']].append(this_event)
                except KeyError:
                    patients.add(this_event['patient'])
                    patient_events[this_event['patient']] = [this_event]
        type1_checks = (self.__AUTO_ANTIBODIES_LABS + 
                       ['lx:c-peptide:threshold:lt:0.8','rx:glucagon','rx:acetone',
                        'dx:diabetes:type-1-not-stated-icd10','dx:diabetes:type-2-uncontrolled',
                        'dx:diabetes:type-1-not-stated','dx:diabetes:type-1-uncontrolled',
                        'dx:diabetes:type-2-not-stated-icd9','dx:diabetes:type-1-not-stated-icd9',
                        'dx:diabetes:type-2-not-stated-icd10','dx:diabetes:type-2-not-stated'] + 
                       self.__ORAL_HYPOGLYCAEMICS)
        type1_qs = Event.objects.filter(name__in=type1_checks, patient__in=patients)
        type1_qs = type1_qs.exclude(case__condition__in=self.__FRANK_DM_CONDITIONS).values('pk','patient','name','date')        
        for event in type1_qs:
            patient_events[event['patient']].append(event)
        patients_with_existing_cases = self.process_existing_cases(patients,patient_events,self.__FRANK_DM_CONDITIONS)
        newpatients = (patients - patients_with_existing_cases)
        funcs = []
        for patient in newpatients:
            f = partial(self._determine_frank_dm_type, patient_events[patient], criteria)
            funcs.append( f )
        return wait_for_threads(funcs)
    
    def _determine_frank_dm_type(self, events, criteria):
        '''
        Determine type of Frank DM and generate a case, based on supplied patient 
            and trigger date.
        @param pat_pk:       Patient record primary key
        @type pat_pk:        Integer
        @param trigger_date: Date on which this patient got diabetes
        @type trigger_date:  DateTime.Date
        @param trigger_event_pks: List of relevant events occurring on trigger date
        @type trigger_event_pks:  [Int, Int, ...]
        @return:             Count of new cases created (always 1)
        @rtype:              Integer
        '''
        #TODO: figure out how existing cases are checked to update type (type2 is default, but more data can convert to type 1)
        #TODO: the value for case_event is set to the first 
        condition = None
        case_event = None
         
        #===============================================================================
        #
        # Criteria for type 1 classification.  If no criteria met, then type 2.
        #
        #===============================================================================
        
        #===============================================================================
        #
        # 1. C-peptide test < 0.8
        #
        #-------------------------------------------------------------------------------
        if any(event for event in events if event['name'] == 'lx:c-peptide:threshold:lt:0.8'):
            c_peptide_lx_thresh = sorted([event for event in events if event['name'] == 'lx:c-peptide:threshold:lt:0.8'], 
                                         key=lambda event: event['date'])
            case_event = c_peptide_lx_thresh[0]
            criteria += ' (C-Peptide result below threshold)'
            condition = 'diabetes:type-1'
            log.debug(criteria)
        
        
        #===============================================================================
        #
        # 2. Diabetes auto-antibodies positive
        #
        #-------------------------------------------------------------------------------
        
        if any(event for event in events if event['name'] in self.__AUTO_ANTIBODIES_LABS):
            aa_pos = sorted([event for event in events if event['name'] in self.__AUTO_ANTIBODIES_LABS], key=lambda event: event['date'])
            case_event = aa_pos[0]
            criteria += ' (Diabetes auto-antibodies positive)'
            condition = 'diabetes:type-1'
            log.debug(criteria)
        
        #===============================================================================
        #
        # 3. Prescription for URINE ACETONE TEST STRIPS (search on keyword:  ACETONE)
        #
        #-------------------------------------------------------------------------------
        if any(event for event in events if event['name'] == 'rx:acetone'):
            acetone_rx = sorted([event for event in events if event['name'] == 'rx:acetone'], key=lambda event: event['date'])
            case_event = acetone_rx[0]
            criteria += '(Acetone Rx)'
            condition = 'diabetes:type-1'
            log.debug(criteria)
        
        #===============================================================================
        #
        # 4/5. Ratio of type 1 : type 2 dx_code >50% and (never prescribed oral hypoglycemic 
        #    medications OR prescription for GLUCAGON)
        #
        #-------------------------------------------------------------------------------
        oral_hypoglycaemic_rx = sorted([event for event in events if event['name'] in self.__ORAL_HYPOGLYCAEMICS], key=lambda event: event['date'])
        glucagon_rx = [event for event in events if event['name'] =='rx:glucagon'] 
        if glucagon_rx or (not oral_hypoglycaemic_rx):
            type_1_dx = [event for event in events if event['name'].startswith('dx:diabetes:type-1')]
            type_2_dx = [event for event in events if event['name'].startswith('dx:diabetes:type-2')]  
            count_1 = len(type_1_dx)
            count_2 = len(type_2_dx)
            # Is there a less convoluted way to express this and still avoid divide-by-zero errors?
            if (count_1 and not count_2) or ( count_2 and ( ( count_1 / count_2 ) > 0.5 ) ):
                case_event = sorted(glucagon_rx + type_1_dx + type_2_dx, key=lambda event: event['date'])[0]
                if glucagon_rx:
                    criteria += ' (More than 50% of dx_codes and glucagon rx)'
                else:
                    criteria += ' (More than 50% of dx_codes, and never prescribed oral hypoglycaemics)'
                condition = 'diabetes:type-1'
                log.debug(criteria)
                
        #===============================================================================
        #
        # No Type 1 criteria met, therefore Type 2
        #
        #-------------------------------------------------------------------------------
        if not condition:
            case_event = sorted(events, key=lambda event: event['date'])[0]
            condition = 'diabetes:type-2'
            log.debug(criteria)
        
        #===============================================================================
        #
        # Generate new case
        #
        #===============================================================================
        case_event_obj = Event.objects.get(pk=case_event['pk'])
        events_objs = []
        for evnt in events:
            this_event = Event.objects.get(pk=evnt['pk'])
            events_objs.append(this_event)
        assert condition   # Sanity check
        assert criteria    # Sanity check
        assert events_objs      # Sanity check
        assert case_event_obj  # Sanity check
        t, new_case = self._create_case_from_event_list(
            condition =  condition,
            criteria = criteria,
            recurrence_interval=None,
            event_obj=case_event_obj,
            relevant_event_names = events_objs,
            )
        new_case.isactive=True
        new_case.save()
        if condition=='diabetes:type-2':
            CaseActiveHistory.create(new_case,
                             'I',
                             case_event_obj.date,
                             'Q',
                             case_event_obj)        
        log.debug('Generated new case: %s (%s)' % (new_case, criteria))
        #determine if a prediabetes case exists for this patient, deactivate it if so
        try:
            precase = Case.objects.get(patient=new_case.patient,condition='diabetes:prediabetes',isactive=True)
            if precase.date >= new_case.date:
                precase.delete()
            else:
                precase.isactive = False
                precase.inactive_date = case_event_obj.date
                CaseActiveHistory.create(precase,
                     'D',
                     case_event_obj.date,
                     'D',
                     case_event_obj)
                precase.save()
        except:
            pass
        return 1  # 1 new case generated
            
    def generate_prediabetes(self):
        log.info('Generating cases of pre diabetes')
        ONCE_CRITERIA = [
            'lx:a1c:range:gte:5.7:lte:6.4',
            'lx:glucose-fasting:range:gte:100:lte:125',
            'lx:ogtt50-fasting:range:gte:100:lte:125',
            'lx:ogtt75-fasting:range:gte:100:lte:125',
            'lx:ogtt100-fasting:range:gte:100:lte:125',
            ]
        TWICE_CRITERIA = [
            'lx:glucose-random:range:gte:140:lt:200',
            ]
        all_criteria = ONCE_CRITERIA + TWICE_CRITERIA
        qs = Event.objects.filter(name__in=TWICE_CRITERIA).values('patient')
        qs = qs.annotate(count=Count('pk'))
        patient_pks = qs.filter(count__gte=2).values_list('patient', flat=True).distinct()
        patient_pks = set(patient_pks)
        criteria = ''
        if patient_pks:
            criteria  = 'Criteria #2: 2 random glucoses >=140 and <200 '
        patient_pks2 = Event.objects.filter(name__in=ONCE_CRITERIA).values_list('patient', flat=True).distinct()
        if (patient_pks2):
            criteria += ' Criteria #1: A1C >= 5.7 <= 6.4 or ( Fasting glucose >= 100 <= 125 )'
        patient_pks |= set( patient_pks2 )
        # Ignore patients who already have a prediabetes case
        patient_pks = patient_pks - set( Case.objects.filter(condition='diabetes:prediabetes').values_list('patient', flat=True) )
        
        #  ignore Known frank diabetes ON THE DAY the patient fulfills above criteria for pre-diabetes OR ANY TIME IN THE PAST 
        condition_exclude = ['diabetes:type-2', 'diabetes:type-1']
        patient_pks = patient_pks - set( Case.objects.filter(condition__in=condition_exclude).values_list('patient', flat=True) )
        total = len(patient_pks)
        counter = 0
        for pat_pk in patient_pks:
            
            event_qs = Event.objects.filter(
                patient = pat_pk,
                name__in = all_criteria,
                ).order_by('date')
            trigger_event = event_qs[0]
            trigger_date = trigger_event.date
            prior_dm_case_qs = Case.objects.filter(
                patient = pat_pk,
                condition__startswith = 'diabetes:',
                date__lte = trigger_date,
                )
            if prior_dm_case_qs.count():
                log.info('Patient already has diabetes, skipping. (%8s / %s)' % (counter, total))
                continue # This patient already has diabetes, and as such does not have prediabetes
            
            new_case = Case(
                patient = trigger_event.patient,
                provider = trigger_event.provider,
                date = trigger_event.date,
                condition =  'diabetes:prediabetes',
                criteria = criteria ,
                source = self.uri,
                )
            new_case.save()
            counter += 1
            new_case.events.set(event_qs)
            new_case.save()
            CaseActiveHistory.create(new_case,
                 'I',
                 trigger_event.date,
                 'Q',
                 trigger_event)
            log.info('Saved new case: %s (%8s / %s)' % (new_case, counter, total))
        return counter
    
    GDM_ONCE = [
            
        'lx:ogtt100-fasting:threshold:gte:126',
        'lx:ogtt75-fasting:threshold:gte:126',
        'lx:ogtt75-fasting:threshold:gte:92',
        'lx:ogtt50-fasting:threshold:gte:126',
        'lx:ogtt50-1hr:threshold:gte:190',
        'lx:ogtt50-random:threshold:gte:190',
        'lx:ogtt75-30min:threshold:gte:200',
        'lx:ogtt75-1hr:threshold:gte:180',
        'lx:ogtt75-90min:threshold:gte:180',
        'lx:ogtt75-2hr:threshold:gte:153',
        ]
    # Two or more occurrences of these events, during pregnancy, is sufficient for a case of GDM
    GDM_TWICE = [
        'lx:ogtt75-fasting:threshold:gte:92',
        'lx:ogtt75-30min:threshold:gte:200',
        'lx:ogtt75-1hr:threshold:gte:180',
        'lx:ogtt75-90min:threshold:gte:180',
        'lx:ogtt75-2hr:threshold:gte:155',
        'lx:ogtt100-fasting-urine:positive',
        'lx:ogtt100-fasting:threshold:gte:95',
        'lx:ogtt100-30min:threshold:gte:200',
        'lx:ogtt100-1hr:threshold:gte:180',
        'lx:ogtt100-90min:threshold:gte:180',
        'lx:ogtt100-2hr:threshold:gte:155',
        'lx:ogtt100-3hr:threshold:gte:140',
        'lx:ogtt100-4hr:threshold:gte:140',
        'lx:ogtt100-5hr:threshold:gte:140',

        ]
    
    def generate_gestational_diabetes(self):
        log.info('Generating cases of gestational diabetes')
        #===============================================================================
        #
        # Build set of GDM pregnancy timespans
        #
        #===============================================================================
        gdm_timespan_pks = set()
        ts_qs = Timespan.objects.filter(name__startswith='pregnancy')
        ts_qs = ts_qs.exclude(case__condition='diabetes:gestational')
        criteria =''
        #
        # Single event
        #
        once_qs = ts_qs.filter(
            patient__event__name__in = self.GDM_ONCE,
            patient__event__date__gte = F('start_date'),
            patient__event__date__lte = F('end_date'),
            ).distinct().order_by('end_date')
        gdm_timespan_pks.update(once_qs.values_list('pk', flat=True))
        if once_qs:
            criteria = 'Criteria #1: Patient pregnant and one of the following: OB Fasting glucose >=126, OGTT50 with single value >=190, OGTT75-intrapartum with >=1 value above threshold'
    
        #
        # 2 or more events
        #
        twice_qs = ts_qs.filter(
            patient__event__name__in = self.GDM_TWICE,
            patient__event__date__gte = F('start_date'),
            patient__event__date__lte = F('end_date'),
            ).annotate(count=Count('patient__event__id')).filter(count__gte=2).distinct()
        gdm_timespan_pks.update(twice_qs.values_list('pk', flat=True))
        if twice_qs:
            criteria += ' Criteria #2: Patient pregnant and one of the following: OGTT75-intrapartum with >=2 value above threshold, OGTT100 with >=2 values above threshold'
    
        #
        # Dx or Rx
        #
        dx_ets=['dx:diabetes:all-types','dx:gestational-diabetes']
        rx_ets=['rx:lancets', 'rx:test-strips']
        _event_qs = Event.objects.filter(
            name__in=rx_ets,
            patient__event__name__in = dx_ets, 
            # date of diagnosis
            patient__event__date__gte = (F('date') - datetime.timedelta(days=14) ),
            patient__event__date__lte = (F('date') + datetime.timedelta(days=14) ),
            )
        # gestational diabetes so this filter below returns empty
        dxrx_qs = ts_qs.filter(
            patient__event__in = _event_qs,
            patient__event__date__gte = F('start_date'),
            patient__event__date__lte = F('end_date'),
            )
        # Currently pregnant patients have a null end date
        dxrx_qs |= ts_qs.filter(
            end_date__isnull=True,
            patient__event__in = _event_qs,
            patient__event__date__gte = F('start_date'),
            )
        if dxrx_qs:
            criteria += ' Criteria #3: Patient pregnant and [dx code for GDM and (prescription containing the term LANCETS or TEST STRIPS)] within 14 days AND no frank diabetes dx code prior to pregnancy start date'
       
        gdm_timespan_pks.update(dxrx_qs.values_list('pk', flat=True))
        #===============================================================================
        #
        # Generate one case per timespan
        #
        #===============================================================================
        all_criteria = self.GDM_ONCE + self.GDM_TWICE + dx_ets + rx_ets
        counter = 0
        total = len(gdm_timespan_pks)
        for ts_pk in gdm_timespan_pks:
            ts = Timespan.objects.get(pk=ts_pk)
            case_events = Event.objects.filter(
                patient = ts.patient,
                name__in=all_criteria,
                date__gte=ts.start_date, 
                )
            if ts.end_date:
                case_events = case_events.filter(date__lte=ts.end_date)
            else:
                case_events = case_events.filter(date__lte=TODAY)
            case_events = case_events.order_by('date')
            first_event = case_events[0]
            case_obj, created = Case.objects.get_or_create(
                patient = ts.patient,
                condition = 'diabetes:gestational',
                date = first_event.date,
                source = self.uri, 
                defaults = {
                    'provider': first_event.provider,
                    },
                )
            case_obj.criteria = criteria
            if created:
                case_obj.save()
                case_obj.events.set(case_events)
                counter += 1
                log.info('Saved new case: %s (%8s / %s)' % (case_obj, counter, total))
            else:
                log.debug('Found exisiting GDM case #%s for %s' % (case_obj.pk, ts))
                log.debug('Timespan & events will be added to existing case')
                case_obj.events.set(case_obj.events.all() | case_events)
            case_obj.timespans.add(ts)
            case_obj.save()
        log.debug('Generated %s new cases of diabetes_gestational' % counter)
        return counter
        
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

diabetes_definition = Diabetes()

def event_heuristics():
    return diabetes_definition.event_heuristics

def timespan_heuristics():
    return []

def disease_definitions():
    return [diabetes_definition]

